# RapidApiCovidSearchExtension

RapidApiCovidSearchExtension is how name says it extension of
https://rapidapi.com/api-sports/api/covid-193 . 
It provides additional layer of search capability.
Main feature is search with period and result as statistic provided
in percentage.

#### Developer Notes

Project is created as Maven Spring Boot Rest Api with Hibernate, Lombok and SpringBoot Security
for JWT support. Model driven design is used as architecture of this project. 
Code first approach and implementation of persistence layer.


## Setup
### Application Properties

Set parameters with your settings:
* *spring.datasource.url* - database server url and name of database
* *spring.datasource.username* - username of database user
* *spring.datasource.password* - password of database user

Optional parameters:
* *rapidApi.ApiKey* - if you have your api key for rapid api use that one, if not leave it as is
* *app.jwtExpirationMs* - represents duration of jwt token in milliseconds, change it as you wish

### Postman

* Import into Postman both files that are placed into postman folder in the root of the project.
* *RapidApi-COVID19.postman_collection* - contains collection of requests to test this api.
* *RapidApi-COVID19.postman_environment* - contains global variables for collection to work.

#### RapidApi-COVID19.postman_environment

Only change this two parameters:
* *x-rapidapi-key* - if you have your api key for rapid api use that one, if not leave it as is
* *LocalBaseUrl* - replace current value with yours local address of this api
