package rapid.api.cse.presentation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.dtos.response.ErrorResponseDto;
import rapid.api.cse.services.search.abstractions.SearchService;
import rapid.api.cse.services.search.exceptions.UnauthorizedException;
import rapid.api.cse.services.validation.exceptions.ValidationException;

import java.time.format.DateTimeParseException;

@CrossOrigin
@RestController
@RequestMapping("/api/search")
@RequiredArgsConstructor
public class SearchController {

    private final SearchService searchService;

    @RequestMapping(method = RequestMethod.GET, value = "/by")
    public ResponseEntity<?> by(@RequestParam() String country, @RequestParam() String dateFrom, @RequestParam() String dateTo) {
        try {
            return new ResponseEntity<>(searchService.by(SearchRequestDto.builder().country(country).dateFrom(dateFrom).dateTo(dateTo).build()), HttpStatus.OK);
        }
        catch (ValidationException validationException) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error(validationException.getMessage()).build(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        catch (UnauthorizedException unauthorizedException) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error(unauthorizedException.getMessage()).build(), HttpStatus.UNAUTHORIZED);
        }
        catch (DateTimeParseException dateTimeParseException) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error("Date is in wrong format! Please use yyyy-MM-dd!").build(), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
