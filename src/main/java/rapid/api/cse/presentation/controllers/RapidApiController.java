package rapid.api.cse.presentation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rapid.api.cse.services.clients.abstractions.CountriesRestClient;
import rapid.api.cse.services.clients.abstractions.HistoryRestClient;
import rapid.api.cse.services.dtos.response.ErrorResponseDto;

@CrossOrigin
@RestController
@RequestMapping("/api/rapidApi")
@RequiredArgsConstructor
public class RapidApiController {

    private final CountriesRestClient countriesRestClient;
    private final HistoryRestClient historyRestClient;

    @RequestMapping(method = RequestMethod.GET, value = "/getCountries")
    public ResponseEntity<?> getCountries(){
        try {
            return new ResponseEntity<>(countriesRestClient.get(), HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getHistory")
    public ResponseEntity<?> getHistory(@RequestParam() String country,@RequestParam() String day ){
        try {
            return new ResponseEntity<>(historyRestClient.get(country,day), HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
