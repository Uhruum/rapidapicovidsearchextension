package rapid.api.cse.presentation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.dtos.response.ErrorResponseDto;
import rapid.api.cse.services.dtos.response.recycle.RecycleResponseDto;
import rapid.api.cse.services.recycling.abstractions.RecyclingService;

import java.time.format.DateTimeParseException;

@CrossOrigin
@RestController
@RequestMapping("/api/recycle")
@RequiredArgsConstructor
public class RecycleController {

    private final RecyclingService recyclingService;

    @RequestMapping(method = RequestMethod.DELETE, value = "/all")
    public ResponseEntity<?> all() {
        try {
            RecycleResponseDto recycleResponseDto = recyclingService.recycleAll();
            if (recycleResponseDto.isSuccessful()) {
                return new ResponseEntity<>(recycleResponseDto, HttpStatus.OK);
            }
            return new ResponseEntity<>(recycleResponseDto, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/by")
    public ResponseEntity<?> by(@RequestParam() String country, @RequestParam() String dateFrom, @RequestParam() String dateTo) {
        try {
            RecycleResponseDto recycleResponseDto = recyclingService.recycleBy(SearchRequestDto.builder().country(country).dateFrom(dateFrom).dateTo(dateTo).build());
            if (recycleResponseDto.isSuccessful()) {
                return new ResponseEntity<>(recycleResponseDto, HttpStatus.OK);
            }
            return new ResponseEntity<>(recycleResponseDto, HttpStatus.BAD_REQUEST);
        }
        catch (DateTimeParseException dateTimeParseException) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error("Date is in wrong format! Please use yyyy-MM-dd!").build(), HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
