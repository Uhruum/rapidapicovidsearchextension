package rapid.api.cse.presentation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rapid.api.cse.services.authentication.abstraction.AuthenticationService;
import rapid.api.cse.services.dtos.request.AuthenticateRequestDto;
import rapid.api.cse.services.dtos.request.RegisterRequestDto;
import rapid.api.cse.services.dtos.response.ErrorResponseDto;
import rapid.api.cse.services.registration.abstractions.RegistrationService;
import rapid.api.cse.services.validation.exceptions.ValidationException;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthenticationController {
    private final RegistrationService registrationService;
    private final AuthenticationService authenticationService;

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequestDto registerRequestDto){
        try {
           return new ResponseEntity<>(registrationService.register(registerRequestDto), HttpStatus.OK);
        }catch (ValidationException validationException){
           return new ResponseEntity<>(ErrorResponseDto.builder().error(validationException.getMessage()).build(),HttpStatus.UNPROCESSABLE_ENTITY);
        }catch (Exception e){
           return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody AuthenticateRequestDto registerRequestDto){
        try {
            return new ResponseEntity<>(authenticationService.authenticate(registerRequestDto),HttpStatus.OK);
        }catch (ValidationException validationException){
            return new ResponseEntity<>(ErrorResponseDto.builder().error(validationException.getMessage()).build(),HttpStatus.UNPROCESSABLE_ENTITY);
        }catch (Exception e){
            return new ResponseEntity<>(ErrorResponseDto.builder().error(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
