package rapid.api.cse.presentation.filters;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import rapid.api.cse.services.authorization.abstractions.AuthorizationTokenExtractor;
import rapid.api.cse.services.authorization.abstractions.JwtService;
import rapid.api.cse.services.authorization.implementation.CseUserDetailsService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
@RequiredArgsConstructor
public class CseRequestFilter extends OncePerRequestFilter {

    private final JwtService jwtService;
    private final CseUserDetailsService cseUserDetailsService;
    private final AuthorizationTokenExtractor authorizationTokenExtractor;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String jwt = authorizationTokenExtractor.extractToken(request);
        if (jwt != null && jwtService.validateToken(jwt)) {
            String username = jwtService.getUserNameFromJwtToken(jwt);
            UserDetails userDetails = cseUserDetailsService.loadUserByUsername(username);
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(request, response);
    }
}
