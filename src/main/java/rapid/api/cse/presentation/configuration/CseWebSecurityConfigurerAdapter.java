package rapid.api.cse.presentation.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import rapid.api.cse.presentation.filters.CseRequestFilter;
import rapid.api.cse.services.authorization.implementation.CseUserDetailsService;

@EnableWebSecurity
@RequiredArgsConstructor
public class CseWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private final CseUserDetailsService cseUserDetailsService;
    private final CseRequestFilter cseRequestFilter;
    private final CseAuthenticationEntryPoint cseAuthenticationEntryPoint;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(cseUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(cseAuthenticationEntryPoint).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/api/auth/**").permitAll()
                .antMatchers("/api/home/**").permitAll()
                .anyRequest().authenticated();
        httpSecurity.addFilterBefore(cseRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
