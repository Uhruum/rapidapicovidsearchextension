package rapid.api.cse.services.clients.abstractions;

import rapid.api.cse.services.clients.exceptions.ResponseEntityStatusCodeNotOkException;
import rapid.api.cse.services.dtos.response.rapidApi.CountriesResponseDto;

public interface CountriesRestClient {
    CountriesResponseDto get() throws ResponseEntityStatusCodeNotOkException;
}
