package rapid.api.cse.services.clients.abstractions;

import rapid.api.cse.services.clients.exceptions.ResponseEntityStatusCodeNotOkException;
import rapid.api.cse.services.dtos.response.rapidApi.HistoryResponseDto;

public interface HistoryRestClient {
    HistoryResponseDto get(String country, String day) throws ResponseEntityStatusCodeNotOkException;
}
