package rapid.api.cse.services.clients.exceptions;

public class ResponseEntityStatusCodeNotOkException extends Exception {
    public ResponseEntityStatusCodeNotOkException(String message) {
        super(message);
    }
}
