package rapid.api.cse.services.clients.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import rapid.api.cse.services.clients.exceptions.ResponseEntityStatusCodeNotOkException;
import rapid.api.cse.services.configuration.RapidApiSettings;
import rapid.api.cse.services.dtos.response.rapidApi.HistoryResponseDto;
import rapid.api.cse.services.providers.httpEntity.abstractions.HttpEntityProvider;

import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class HistoryRestClientImpl implements rapid.api.cse.services.clients.abstractions.HistoryRestClient {

    private final RapidApiSettings _rapidApiSettings;
    private final HttpEntityProvider<HttpEntity<HistoryResponseDto>> _httpEntityProvider;

    @Override
    public HistoryResponseDto get(String country, String day) throws ResponseEntityStatusCodeNotOkException {
        RestTemplate rest = new RestTemplate();
        String urlTemplate = UriComponentsBuilder.fromHttpUrl(_rapidApiSettings.getBaseUrl() + "/history")
                .queryParam("country", "{country}")
                .queryParam("day", "{day}")
                .encode()
                .toUriString();

        Map<String, String> params = new HashMap<>();
        params.put("country", country);
        params.put("day", day);

        ResponseEntity<HistoryResponseDto> response = rest.exchange(urlTemplate, HttpMethod.GET, _httpEntityProvider.provide(), HistoryResponseDto.class, params);
        if(response.getStatusCode() == HttpStatus.OK)
            return response.getBody();

        throw new ResponseEntityStatusCodeNotOkException(String.format("HistoryRestClient returned %s", response.getStatusCode()));
    }

}
