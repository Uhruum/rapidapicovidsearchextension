package rapid.api.cse.services.clients.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import rapid.api.cse.services.clients.exceptions.ResponseEntityStatusCodeNotOkException;
import rapid.api.cse.services.configuration.RapidApiSettings;
import rapid.api.cse.services.dtos.response.rapidApi.CountriesResponseDto;
import rapid.api.cse.services.providers.httpEntity.abstractions.HttpEntityProvider;


@Component
@RequiredArgsConstructor
public class CountriesRestClientImpl implements rapid.api.cse.services.clients.abstractions.CountriesRestClient {

    private final RapidApiSettings _rapidApiSettings;
    private final HttpEntityProvider<HttpEntity<CountriesResponseDto>> _httpEntityProvider;

    public CountriesResponseDto get() throws ResponseEntityStatusCodeNotOkException {
        RestTemplate rest = new RestTemplate();
        HttpEntity<CountriesResponseDto> requestEntity = _httpEntityProvider.provide();
        ResponseEntity<CountriesResponseDto> response = rest.exchange(_rapidApiSettings.getBaseUrl() + "/countries", HttpMethod.GET, requestEntity, CountriesResponseDto.class);

        if(response.getStatusCode() == HttpStatus.OK)
            return response.getBody();

        throw new ResponseEntityStatusCodeNotOkException(String.format("CountriesRestClient returned %s", response.getStatusCode()));
    }

}
