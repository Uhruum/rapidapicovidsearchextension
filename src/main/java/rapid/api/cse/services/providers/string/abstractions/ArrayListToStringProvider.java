package rapid.api.cse.services.providers.string.abstractions;

import java.util.ArrayList;

public interface ArrayListToStringProvider {
    String provide(ArrayList<String> list);
}
