package rapid.api.cse.services.providers.httpHeaders.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;
import rapid.api.cse.services.configuration.RapidApiSettings;
import rapid.api.cse.services.providers.httpHeaders.abstractions.HttpHeadersProvider;

@Component
@RequiredArgsConstructor
public class RapidApiHttpHeadersProviderImpl implements HttpHeadersProvider {

    private final RapidApiSettings rapidApiSettings;

    @Override
    public HttpHeaders provide() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.ACCEPT, "*/*");
        headers.add(rapidApiSettings.getHeaderNameForHost(), rapidApiSettings.getHost());
        headers.add(rapidApiSettings.getHeaderNameForApiKey(), rapidApiSettings.getApiKey());
        return headers;
    }

}
