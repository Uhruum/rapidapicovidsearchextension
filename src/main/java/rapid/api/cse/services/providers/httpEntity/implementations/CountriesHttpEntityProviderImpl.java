package rapid.api.cse.services.providers.httpEntity.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import rapid.api.cse.services.dtos.response.rapidApi.CountriesResponseDto;
import rapid.api.cse.services.providers.httpEntity.abstractions.HttpEntityProvider;
import rapid.api.cse.services.providers.httpHeaders.abstractions.HttpHeadersProvider;

@Component
@RequiredArgsConstructor
public class CountriesHttpEntityProviderImpl implements HttpEntityProvider<HttpEntity<CountriesResponseDto>> {

    private final HttpHeadersProvider rapidApiHttpHeadersProvider;

    @Override
    public HttpEntity<CountriesResponseDto> provide() {
        return new HttpEntity<>(null, rapidApiHttpHeadersProvider.provide());
    }

}
