package rapid.api.cse.services.providers.httpEntity.abstractions;

public interface HttpEntityProvider<T> {
    T provide();
}
