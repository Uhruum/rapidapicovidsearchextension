package rapid.api.cse.services.providers.string.implementations;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
@Component
public class ArrayListToStringProviderImpl implements rapid.api.cse.services.providers.string.abstractions.ArrayListToStringProvider {
    @Override
    public String provide(ArrayList<String> list){
        StringBuilder stringBuilder = new StringBuilder();
        list.forEach(message -> stringBuilder.append(message).append(";"));
        return stringBuilder.toString();
    }
}
