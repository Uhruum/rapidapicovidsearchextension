package rapid.api.cse.services.providers.httpEntity.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import rapid.api.cse.services.dtos.response.rapidApi.HistoryResponseDto;
import rapid.api.cse.services.providers.httpEntity.abstractions.HttpEntityProvider;
import rapid.api.cse.services.providers.httpHeaders.abstractions.HttpHeadersProvider;

@Component
@RequiredArgsConstructor
public class HistoryHttpEntityProviderImpl implements HttpEntityProvider<HttpEntity<HistoryResponseDto>> {

    private final HttpHeadersProvider rapidApiHttpHeadersProvider;

    @Override
    public HttpEntity<HistoryResponseDto> provide() {
        return new HttpEntity<>(null, rapidApiHttpHeadersProvider.provide());
    }

}
