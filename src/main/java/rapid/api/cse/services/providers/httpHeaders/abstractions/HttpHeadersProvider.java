package rapid.api.cse.services.providers.httpHeaders.abstractions;

import org.springframework.http.HttpHeaders;

public interface HttpHeadersProvider {
    HttpHeaders provide();
}
