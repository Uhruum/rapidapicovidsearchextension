package rapid.api.cse.services.authentication.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.services.repositories.ApplicationUserRepository;
import rapid.api.cse.services.authorization.abstractions.JwtService;
import rapid.api.cse.services.dtos.request.AuthenticateRequestDto;
import rapid.api.cse.services.dtos.response.AuthenticateResponseDto;
import rapid.api.cse.services.providers.string.abstractions.ArrayListToStringProvider;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.exceptions.ValidationException;
import rapid.api.cse.services.validation.result.ValidationResult;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements rapid.api.cse.services.authentication.abstraction.AuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final Validator<AuthenticateRequestDto> requestValidator;
    private final ApplicationUserRepository applicationUserRepository;
    private final JwtService jwtService;
    private final ArrayListToStringProvider arrayListToStringProvider;

    @Override
    public AuthenticateResponseDto authenticate(AuthenticateRequestDto authenticateRequestDto) throws ValidationException {

        ValidationResult validationResult = requestValidator.validate(authenticateRequestDto);
        if (!validationResult.getIsValid())
            throw new ValidationException(arrayListToStringProvider.provide(validationResult.getMessages()));

        Optional<ApplicationUser> optionalApplicationUser = applicationUserRepository.findByUsername(authenticateRequestDto.getUsername());
        if(optionalApplicationUser.isEmpty())
            throw new ValidationException("Invalid email or password!");

        ApplicationUser applicationUser = optionalApplicationUser.get();
        if (!passwordEncoder.matches(authenticateRequestDto.getPassword(), applicationUser.getPassword()))
            throw new ValidationException("Invalid email or password!");

        String jwtToken = jwtService.generateJwtToken(applicationUser.getUsername());
        return AuthenticateResponseDto.builder().token(jwtToken).build();
    }

}
