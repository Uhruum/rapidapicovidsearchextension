package rapid.api.cse.services.authentication.abstraction;

import rapid.api.cse.services.dtos.request.AuthenticateRequestDto;
import rapid.api.cse.services.dtos.response.AuthenticateResponseDto;
import rapid.api.cse.services.validation.exceptions.ValidationException;

public interface AuthenticationService {
    AuthenticateResponseDto authenticate(AuthenticateRequestDto authenticateRequestDto) throws ValidationException;
}
