package rapid.api.cse.services.mappers.apstractions;

import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.domain.enities.Search;
import rapid.api.cse.services.dtos.request.SearchRequestDto;

public interface SearchRequestDtoToSearchMapper {
    Search map(SearchRequestDto searchRequestDto, Country country, ApplicationUser applicationUser);
}
