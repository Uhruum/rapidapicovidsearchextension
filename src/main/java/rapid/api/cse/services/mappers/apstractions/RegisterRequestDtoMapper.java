package rapid.api.cse.services.mappers.apstractions;

import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.services.dtos.request.RegisterRequestDto;


public interface RegisterRequestDtoMapper {
    ApplicationUser map(RegisterRequestDto registerRequestDto);
}
