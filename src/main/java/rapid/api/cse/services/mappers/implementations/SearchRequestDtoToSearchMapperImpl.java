package rapid.api.cse.services.mappers.implementations;

import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.domain.enities.Search;
import rapid.api.cse.services.dtos.request.SearchRequestDto;

@Component
public class SearchRequestDtoToSearchMapperImpl implements rapid.api.cse.services.mappers.apstractions.SearchRequestDtoToSearchMapper {
    @Override
    public Search map(SearchRequestDto searchRequestDto, Country country, ApplicationUser applicationUser){
        return new Search(searchRequestDto.getDateFrom(), searchRequestDto.getDateTo(), country, applicationUser);
    }
}
