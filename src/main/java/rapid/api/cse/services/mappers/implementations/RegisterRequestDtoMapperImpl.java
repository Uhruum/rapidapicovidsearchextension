package rapid.api.cse.services.mappers.implementations;

import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.services.dtos.request.RegisterRequestDto;

@Component
public class RegisterRequestDtoMapperImpl implements rapid.api.cse.services.mappers.apstractions.RegisterRequestDtoMapper {

    @Override
    public ApplicationUser map(RegisterRequestDto registerRequestDto) {
        return new ApplicationUser(registerRequestDto.getUsername(), registerRequestDto.getPassword(), registerRequestDto.getFirstName(), registerRequestDto.getLastName());
    }
}
