package rapid.api.cse.services.validation.abstraction;
import rapid.api.cse.services.validation.result.ValidationResult;

public interface Validator<T>{
    ValidationResult validate(T t);
}
