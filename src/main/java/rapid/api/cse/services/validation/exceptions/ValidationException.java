package rapid.api.cse.services.validation.exceptions;

public class ValidationException extends Exception{
    public ValidationException(String message) {
        super(message);
    }
}
