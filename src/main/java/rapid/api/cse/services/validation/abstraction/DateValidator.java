package rapid.api.cse.services.validation.abstraction;

public interface DateValidator {
    boolean isValid(String date);
}
