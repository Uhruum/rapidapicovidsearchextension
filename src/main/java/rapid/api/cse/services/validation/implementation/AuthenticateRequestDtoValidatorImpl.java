package rapid.api.cse.services.validation.implementation;


import org.springframework.stereotype.Component;
import rapid.api.cse.services.dtos.request.AuthenticateRequestDto;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.result.ValidationResult;

import java.util.ArrayList;

@Component
public class AuthenticateRequestDtoValidatorImpl implements Validator<AuthenticateRequestDto> {

    @Override
    public ValidationResult validate(AuthenticateRequestDto authenticateRequestDto) {
        ArrayList<String> messages = new ArrayList<>();
        if (authenticateRequestDto.getPassword().isBlank()
                || authenticateRequestDto.getPassword().isEmpty()
                || authenticateRequestDto.getPassword() == null)
            messages.add("Password is required!");

        if (authenticateRequestDto.getUsername().isBlank()
                || authenticateRequestDto.getUsername().isEmpty()
                || authenticateRequestDto.getUsername() == null)
            messages.add("Invalid username!");

        return ValidationResult.builder()
                .messages(messages)
                .isValid(messages.size() == 0)
                .build();
    }

}
