package rapid.api.cse.services.validation.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.services.repositories.ApplicationUserRepository;
import rapid.api.cse.services.dtos.request.RegisterRequestDto;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.result.ValidationResult;

import java.util.ArrayList;

@Component
@RequiredArgsConstructor
public class RegisterRequestDtoValidatorImpl implements Validator<RegisterRequestDto> {

    private final ApplicationUserRepository applicationUserRepository;

    @Override
    public ValidationResult validate(RegisterRequestDto registerRequestDto) {
        ArrayList<String> messages = new ArrayList<>();

        if (registerRequestDto.getPassword().isBlank()
                || registerRequestDto.getPassword().isEmpty()
                || registerRequestDto.getPassword() == null)
            messages.add("Password is required!");
        else {
            if (registerRequestDto.getPassword().length() > 10)
                messages.add("Password is to big! Max length is 10.");
        }

        if (registerRequestDto.getUsername().isBlank()
                || registerRequestDto.getUsername().isEmpty()
                || registerRequestDto.getUsername() == null)
            messages.add("Invalid username!");
        else {
            if (applicationUserRepository.findByUsername(registerRequestDto.getUsername()).isPresent())
                messages.add("User with that username exists! Username already taken!");
        }

        if (registerRequestDto.getFirstName().length() > 30)
            messages.add("FirstName is to big! Max length is 30.");

        if (registerRequestDto.getLastName().length() > 50)
            messages.add("LastName is to big! Max length is 50.");

        return ValidationResult.builder()
                .messages(messages)
                .isValid(messages.size() == 0)
                .build();
    }

}
