package rapid.api.cse.services.validation.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
@Component
@RequiredArgsConstructor
public class DateValidatorImpl implements rapid.api.cse.services.validation.abstraction.DateValidator {
    private final DateTimeFormatter dateFormatter;

    public boolean isValid(String date){
        try {
            LocalDate.parse(date, dateFormatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }
}
