package rapid.api.cse.services.validation.result;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
@Data
@Builder
public class ValidationResult {
    private ArrayList<String> messages;
    private Boolean isValid;
}
