package rapid.api.cse.services.validation.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.services.repositories.CountryRepository;
import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.validation.abstraction.DateValidator;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.result.ValidationResult;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SearchRequestDtoValidatorImpl implements Validator<SearchRequestDto> {

    private final DateValidator dateValidator;
    private final DateTimeFormatter dateFormatter;
    private final CountryRepository countryRepository;

    @Override
    public ValidationResult validate(SearchRequestDto searchRequestDto) {
        ArrayList<String> messages = new ArrayList<>();
        if (!dateValidator.isValid(searchRequestDto.getDateFrom())) {
            messages.add("Invalid date from! It must be date, cannot be null or empty!");
        }

        if (!dateValidator.isValid(searchRequestDto.getDateTo())) {
            messages.add("Invalid date to! It must be date, cannot be null or empty!");
        }

        LocalDate from = LocalDate.parse(searchRequestDto.getDateFrom(), dateFormatter);
        LocalDate to = LocalDate.parse(searchRequestDto.getDateTo(), dateFormatter);

        if(from.isAfter(to))
            messages.add("Date from needs to be before date to!");

        if(to.isBefore(from))
            messages.add("Date to needs to be after date to!");

        Optional<Country> optionalCountry = countryRepository.findByName(searchRequestDto.getCountry());
        if (optionalCountry.isEmpty())
            messages.add("Invalid country name! Country name should look like 'Croatia', please check GetCountries method for correct name of an country!");

        return ValidationResult.builder()
                .messages(messages)
                .isValid(messages.size() == 0)
                .build();
    }

}
