package rapid.api.cse.services.recycling.abstractions;

import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.dtos.response.recycle.RecycleResponseDto;

public interface RecyclingService {
    RecycleResponseDto recycleAll();
    RecycleResponseDto recycleBy(SearchRequestDto searchRequestDto);
}
