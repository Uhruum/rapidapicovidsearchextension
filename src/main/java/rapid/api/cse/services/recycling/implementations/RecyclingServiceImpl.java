package rapid.api.cse.services.recycling.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.domain.enities.Search;
import rapid.api.cse.domain.enities.SearchResult;
import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.repositories.CountryRepository;
import rapid.api.cse.services.repositories.SearchRepository;
import rapid.api.cse.services.repositories.SearchResultRepository;
import rapid.api.cse.services.repositories.SearchResultStatisticRepository;
import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.dtos.response.recycle.RecycleResponseDto;
import rapid.api.cse.services.providers.string.abstractions.ArrayListToStringProvider;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.exceptions.ValidationException;
import rapid.api.cse.services.validation.result.ValidationResult;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

@Transactional
@Service
@RequiredArgsConstructor
public class RecyclingServiceImpl implements rapid.api.cse.services.recycling.abstractions.RecyclingService {

    private final SearchRepository searchRepository;
    private final SearchResultRepository searchResultRepository;
    private final SearchResultStatisticRepository searchResultStatisticRepository;
    private final Validator<SearchRequestDto> validator;
    private final CountryRepository countryRepository;
    private final ArrayListToStringProvider arrayListToStringProvider;

    @Override
    public RecycleResponseDto recycleAll(){
        try {
            searchResultRepository.deleteAll();
            searchResultStatisticRepository.deleteAll();
            searchRepository.deleteAll();
            return RecycleResponseDto.builder().message("Successfully deleted all data!").isSuccessful(true).build();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return RecycleResponseDto.builder().message("Failed to deleted all data!").isSuccessful(false).build();
        }
    }

    @Override
    public RecycleResponseDto recycleBy(SearchRequestDto searchRequestDto){
        try{
            ValidationResult validationResult = validator.validate(searchRequestDto);
            if (!validationResult.getIsValid())
                throw new ValidationException(arrayListToStringProvider.provide(validationResult.getMessages()));
            Optional<Country> optionalCountry = countryRepository.findByName(searchRequestDto.getCountry());
            if(optionalCountry.isEmpty())
                throw new ValidationException("Invalid country name! Country name should look like 'Croatia', please check GetCountries method for correct name of an country!");

            ArrayList<SearchResult> searchResultsForDelete = new ArrayList<>();
            ArrayList<SearchResultStatistic> searchResultStatisticsForDelete = new ArrayList<>();
            ArrayList<Search> searchesForDelete;
            searchesForDelete = new ArrayList<>(searchRepository.getAllByDateFromAndDateToAndCountry(searchRequestDto.getDateFrom(), searchRequestDto.getDateTo(), optionalCountry.get()));

            if(searchesForDelete.size() == 0){
                return RecycleResponseDto.builder().message("Nothing to delete by params dateFrom "
                        + searchRequestDto.getDateFrom()
                        + " dateTo "+ searchRequestDto.getDateTo()
                        + " and country "+searchRequestDto.getCountry()+ "!").isSuccessful(true).build();
            }

            searchesForDelete.forEach(search -> {
                Set<SearchResult> results = search.getResults();
                searchResultsForDelete.addAll(results);
                searchResultsForDelete.forEach(searchResult -> searchResultStatisticsForDelete.add(searchResult.getStatistic()));
            });

            searchResultRepository.deleteAll(searchResultsForDelete);
            searchResultStatisticRepository.deleteAll(searchResultStatisticsForDelete);
            searchRepository.deleteAll(searchesForDelete);
            return RecycleResponseDto.builder().message("Successfully deleted all data for specific search with params by dateFrom "
                    + searchRequestDto.getDateFrom()
                    + " dateTo "+ searchRequestDto.getDateTo()
                    + " and country "+searchRequestDto.getCountry()+ "!").isSuccessful(true).build();
        }
        catch (ValidationException validationException){
            return RecycleResponseDto.builder().message(validationException.getMessage()).isSuccessful(false).build();
        }
        catch (Exception e)
        {
            return RecycleResponseDto.builder().message("Failed to deleted all data by dateFrom "
                    + searchRequestDto.getDateFrom()
                    + " dateTo "+ searchRequestDto.getDateTo()
                    + " and country "+searchRequestDto.getCountry()+ "!").isSuccessful(false).build();
        }
    }

}
