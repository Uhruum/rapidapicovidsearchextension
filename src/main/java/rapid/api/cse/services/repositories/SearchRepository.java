package rapid.api.cse.services.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.domain.enities.Search;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface SearchRepository extends JpaRepository<Search, Long> {
    List<Search> getAllByDateFromAndDateToAndCountry(String dateFrom, String dateTo, Country country);
}
