package rapid.api.cse.services.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rapid.api.cse.domain.enities.SearchResultStatistic;

@Repository
public interface SearchResultStatisticRepository extends JpaRepository<SearchResultStatistic, Long> {

}
