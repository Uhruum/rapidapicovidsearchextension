package rapid.api.cse.services.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.domain.enities.SearchResult;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface SearchResultRepository extends JpaRepository<SearchResult, Long> {
    List<SearchResult> findAllByDateAndCountry(String date, Country country);
}
