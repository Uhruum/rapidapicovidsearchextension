package rapid.api.cse.services.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;

@Configuration
public class ApplicationBeanConfiguration {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    public RapidApiSettings rapidApiSettings() {
        return new RapidApiSettings();
    }
    @Bean
    public DateTimeFormatter dateTimeFormatter(){
        return DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }
    @Bean
    public DecimalFormat decimalFormat(){return new DecimalFormat("0.00");}
    @Bean
    public JwtSettings jwtSettings() {
        return new JwtSettings();
    }
}
