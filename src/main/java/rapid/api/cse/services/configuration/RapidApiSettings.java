package rapid.api.cse.services.configuration;

import org.springframework.beans.factory.annotation.Value;

public class RapidApiSettings {
    @Value("${rapidApi.Host}")
    private String host;
    @Value("${rapidApi.BaseUrl}")
    private String baseUrl;
    @Value("${rapidApi.ApiKey}")
    private String apiKey;
    @Value("${rapidApi.HeaderNameForApiKey}")
    private String headerNameForApiKey;
    @Value("${rapidApi.HeaderNameForHost}")
    private String headerNameForHost;

    public String getHost() {
        return host;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getHeaderNameForApiKey() {
        return headerNameForApiKey;
    }

    public String getHeaderNameForHost() {
        return headerNameForHost;
    }
}
