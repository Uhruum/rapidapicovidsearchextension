package rapid.api.cse.services.registration.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.services.repositories.ApplicationUserRepository;
import rapid.api.cse.services.authorization.abstractions.JwtService;
import rapid.api.cse.services.dtos.request.RegisterRequestDto;
import rapid.api.cse.services.dtos.response.RegistrationResponseDto;
import rapid.api.cse.services.mappers.apstractions.RegisterRequestDtoMapper;
import rapid.api.cse.services.providers.string.abstractions.ArrayListToStringProvider;
import rapid.api.cse.services.validation.exceptions.ValidationException;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.result.ValidationResult;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements rapid.api.cse.services.registration.abstractions.RegistrationService {

    private final RegisterRequestDtoMapper registerRequestDtoMapper;
    private final PasswordEncoder passwordEncoder;
    private final Validator<RegisterRequestDto> requestValidator;
    private final ApplicationUserRepository applicationUserRepository;
    private final JwtService jwtService;
    private final ArrayListToStringProvider arrayListToStringProvider;

    @Override
    public RegistrationResponseDto register(RegisterRequestDto registerRequestDto) throws ValidationException {

        ValidationResult validationResult = requestValidator.validate(registerRequestDto);
        if (!validationResult.getIsValid())
            throw new ValidationException(arrayListToStringProvider.provide(validationResult.getMessages()));

        ApplicationUser applicationUser = registerRequestDtoMapper.map(registerRequestDto);
        String encodedPassword = passwordEncoder.encode(registerRequestDto.getPassword());
        applicationUser.setPassword(encodedPassword);

        ApplicationUser newApplicationUser = applicationUserRepository.save(applicationUser);
        String jwtToken = jwtService.generateJwtToken(newApplicationUser.getUsername());
        return RegistrationResponseDto.builder().token(jwtToken).build();
    }

}
