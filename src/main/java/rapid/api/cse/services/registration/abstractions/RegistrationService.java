package rapid.api.cse.services.registration.abstractions;

import rapid.api.cse.services.dtos.request.RegisterRequestDto;
import rapid.api.cse.services.dtos.response.RegistrationResponseDto;
import rapid.api.cse.services.validation.exceptions.ValidationException;


public interface RegistrationService {
    RegistrationResponseDto register(RegisterRequestDto registerRequestDto) throws ValidationException;
}
