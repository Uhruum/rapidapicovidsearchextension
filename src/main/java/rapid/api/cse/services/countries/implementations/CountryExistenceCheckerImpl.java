package rapid.api.cse.services.countries.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.services.repositories.CountryRepository;

@Component
@RequiredArgsConstructor
public class CountryExistenceCheckerImpl implements rapid.api.cse.services.countries.abstractions.CountryExistenceChecker {

    private final CountryRepository countryRepository;

    public boolean exists(String name){
        return countryRepository.findByName(name).isPresent();
    }

}
