package rapid.api.cse.services.countries.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.services.repositories.CountryRepository;
import rapid.api.cse.services.clients.abstractions.CountriesRestClient;
import rapid.api.cse.services.clients.exceptions.ResponseEntityStatusCodeNotOkException;
import rapid.api.cse.services.countries.abstractions.CountryExistenceChecker;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class CountriesServiceImpl implements rapid.api.cse.services.countries.abstractions.CountriesService {

    private final CountriesRestClient countriesRestClient;
    private final CountryExistenceChecker countryExistenceChecker;
    private final CountryRepository countryRepository;

    @Override
    public void updateCountries(){
        try {
            ArrayList<Country> newCountries = new ArrayList<>();
            ArrayList<String> countries = countriesRestClient.get().getResponse();
            for (String country : countries) {
                if (!countryExistenceChecker.exists(country)) {
                    newCountries.add(new Country(country));
                }
            }

            if(!newCountries.isEmpty())
                countryRepository.saveAll(newCountries);

        } catch (ResponseEntityStatusCodeNotOkException e) {
            e.printStackTrace();
        }
    }

}
