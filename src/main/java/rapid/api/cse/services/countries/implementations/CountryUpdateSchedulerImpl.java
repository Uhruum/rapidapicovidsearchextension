package rapid.api.cse.services.countries.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import rapid.api.cse.services.countries.abstractions.CountriesService;

@Component
@RequiredArgsConstructor
public class CountryUpdateSchedulerImpl {

    private final CountriesService countriesService;

    @Scheduled(fixedRate = 3600000)
    public void scheduleCountriesUpdate() {
        countriesService.updateCountries();
    }
}
