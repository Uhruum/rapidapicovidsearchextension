package rapid.api.cse.services.countries.abstractions;

public interface CountryExistenceChecker {
    boolean exists(String name);
}
