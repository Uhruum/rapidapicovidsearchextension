package rapid.api.cse.services.authorization.abstractions;

import javax.servlet.http.HttpServletRequest;

public interface AuthorizationTokenExtractor {
    String extractToken(HttpServletRequest request);
}
