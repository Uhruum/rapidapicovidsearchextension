package rapid.api.cse.services.authorization.implementation;

import java.util.Date;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import rapid.api.cse.services.configuration.JwtSettings;


@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements rapid.api.cse.services.authorization.abstractions.JwtService {

    private final JwtSettings jwtSettings;

    @Override
    public String generateJwtToken(String username) {
        return doGenerateJwtToken(username);
    }

    private String doGenerateJwtToken(String username){
       return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtSettings.getJwtExpirationMs()))
                .signWith(SignatureAlgorithm.HS512, jwtSettings.getJwtSecret())
                .compact();
    }

    @Override
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSettings.getJwtSecret()).parseClaimsJws(token).getBody().getSubject();
    }

    @Override
    public Boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSettings.getJwtSecret()).parseClaimsJws(token);
            return true;
        } catch(Exception e){
            return false;
        }
    }

    @Override
    public String getUsername(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return userDetails.getUsername();
    }

}
