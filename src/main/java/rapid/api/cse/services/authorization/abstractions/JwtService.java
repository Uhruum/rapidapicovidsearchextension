package rapid.api.cse.services.authorization.abstractions;

public interface JwtService {
    String generateJwtToken(String username);
    String getUserNameFromJwtToken(String token);
    Boolean validateToken(String token);
    String getUsername();
}
