package rapid.api.cse.services.authorization.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.services.repositories.ApplicationUserRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CseUserDetailsService implements UserDetailsService {

    private final ApplicationUserRepository applicationUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ApplicationUser> applicationUser = applicationUserRepository.findByUsername(username);
        if(applicationUser.isPresent())
        {
            ApplicationUser user = applicationUser.get();
            return User.builder().username(user.getUsername()).password(user.getPassword()).authorities(new ArrayList<>()).build();
        }
        else
            throw new UsernameNotFoundException("Username not found!");
    }

}
