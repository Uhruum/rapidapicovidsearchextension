package rapid.api.cse.services.authorization.implementation;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
@Service
public class AuthorizationTokenExtractorImpl implements rapid.api.cse.services.authorization.abstractions.AuthorizationTokenExtractor {

    @Override
    public String extractToken(HttpServletRequest request)  {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }
        return null;
    }
}
