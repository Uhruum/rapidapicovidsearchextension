package rapid.api.cse.services.search.abstractions;

import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.dtos.response.search.SearchResponseDto;
import rapid.api.cse.services.search.exceptions.UnauthorizedException;
import rapid.api.cse.services.validation.exceptions.ValidationException;

public interface SearchService {
    SearchResponseDto by(SearchRequestDto searchRequestDto) throws ValidationException, UnauthorizedException;
}
