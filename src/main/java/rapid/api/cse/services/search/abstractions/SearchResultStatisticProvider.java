package rapid.api.cse.services.search.abstractions;

import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.dtos.response.rapidApi.CasesResponseDto;

public interface SearchResultStatisticProvider {
    SearchResultStatistic provide(CasesResponseDto casesResponseDto);
}
