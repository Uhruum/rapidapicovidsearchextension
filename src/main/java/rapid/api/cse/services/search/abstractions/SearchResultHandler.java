package rapid.api.cse.services.search.abstractions;

import rapid.api.cse.domain.enities.SearchResult;

public interface SearchResultHandler {
    SearchResult handle(SearchResult searchResult);
}
