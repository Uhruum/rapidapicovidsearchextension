package rapid.api.cse.services.search.abstractions;

import rapid.api.cse.domain.enities.SearchResult;
import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.dtos.response.search.StatisticResponseDto;

public interface StatisticResponseDtoProvider {
    StatisticResponseDto provide(SearchResultStatistic searchResultStatistic, SearchResult searchResult);
}
