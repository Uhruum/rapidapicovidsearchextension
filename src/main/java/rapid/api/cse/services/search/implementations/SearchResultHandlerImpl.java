package rapid.api.cse.services.search.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.SearchResult;
import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.repositories.SearchResultRepository;
import rapid.api.cse.services.repositories.SearchResultStatisticRepository;


@Component
@RequiredArgsConstructor
public class SearchResultHandlerImpl implements rapid.api.cse.services.search.abstractions.SearchResultHandler {

    private final SearchResultRepository searchResultRepository;
    private final SearchResultStatisticRepository searchResultStatisticRepository;

    @Override
    public SearchResult handle(SearchResult searchResult) {
        SearchResultStatistic statistic = searchResult.getStatistic();
        searchResultStatisticRepository.save(statistic);
        searchResult.setStatistic(statistic);
        return searchResultRepository.save(searchResult);
    }

}
