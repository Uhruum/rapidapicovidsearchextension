package rapid.api.cse.services.search.exceptions;

public class SearchResultProviderException extends Exception{
    public SearchResultProviderException(String message) {
        super(message);
    }
}
