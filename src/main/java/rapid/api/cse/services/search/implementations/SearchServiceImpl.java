package rapid.api.cse.services.search.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rapid.api.cse.domain.enities.ApplicationUser;
import rapid.api.cse.domain.enities.Country;
import rapid.api.cse.domain.enities.Search;
import rapid.api.cse.domain.enities.SearchResult;
import rapid.api.cse.services.authorization.abstractions.JwtService;
import rapid.api.cse.services.dtos.request.SearchRequestDto;
import rapid.api.cse.services.dtos.response.search.SearchResponseDto;
import rapid.api.cse.services.dtos.response.search.StatisticResponseDto;
import rapid.api.cse.services.mappers.apstractions.SearchRequestDtoToSearchMapper;
import rapid.api.cse.services.providers.string.abstractions.ArrayListToStringProvider;
import rapid.api.cse.services.repositories.ApplicationUserRepository;
import rapid.api.cse.services.repositories.CountryRepository;
import rapid.api.cse.services.repositories.SearchRepository;
import rapid.api.cse.services.search.abstractions.DateHandler;
import rapid.api.cse.services.search.abstractions.SearchResultHandler;
import rapid.api.cse.services.search.abstractions.SearchResultProvider;
import rapid.api.cse.services.search.abstractions.StatisticResponseDtoProvider;
import rapid.api.cse.services.search.exceptions.UnauthorizedException;
import rapid.api.cse.services.validation.abstraction.Validator;
import rapid.api.cse.services.validation.exceptions.ValidationException;
import rapid.api.cse.services.validation.result.ValidationResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements rapid.api.cse.services.search.abstractions.SearchService {

    private final Validator<SearchRequestDto> validator;
    private final CountryRepository countryRepository;
    private final SearchRequestDtoToSearchMapper searchRequestDtoToSearchMapper;
    private final SearchRepository searchRepository;
    private final ApplicationUserRepository applicationUserRepository;
    private final JwtService jwtService;
    private final DateHandler dateHandler;
    private final SearchResultProvider searchResultProvider;
    private final StatisticResponseDtoProvider statisticResponseDtoProvider;
    private final SearchResultHandler searchResultHandler;
    private final ArrayListToStringProvider arrayListToStringProvider;


    @Override
    public SearchResponseDto by(SearchRequestDto searchRequestDto) throws ValidationException, UnauthorizedException {
        ValidationResult validationResult = validator.validate(searchRequestDto);
        if (!validationResult.getIsValid())
            throw new ValidationException(arrayListToStringProvider.provide(validationResult.getMessages()));
        Optional<Country> optionalCountry = countryRepository.findByName(searchRequestDto.getCountry());
        if(optionalCountry.isEmpty())
            throw new ValidationException("Invalid country name! Country name should look like 'Croatia', please check GetCountries method for correct name of an country!");

        Optional<ApplicationUser> applicationUserOptional = applicationUserRepository.findByUsername(jwtService.getUsername());
        if (applicationUserOptional.isEmpty())
            throw new UnauthorizedException("Could not resolve user! Please authenticate!");

        Search search = searchRequestDtoToSearchMapper.map(searchRequestDto, optionalCountry.get(),
                applicationUserOptional.get());
        Search savedSearch = searchRepository.save(search);

        List<String> datesBetween = dateHandler.getDatesBetween(savedSearch.getDateFrom(), savedSearch.getDateTo());
        ArrayList<Optional<SearchResult>> results = new ArrayList<>();
        datesBetween.forEach(date -> results.add(searchResultProvider.provide(savedSearch, date)));

        ArrayList<StatisticResponseDto> statisticResponseDtoArrayList = new ArrayList<>();
        for (Optional<SearchResult> optionalSearchResult : results) {
            optionalSearchResult.ifPresent(searchResult -> statisticResponseDtoArrayList.add(statisticResponseDtoProvider
                    .provide(searchResultHandler
                            .handle(searchResult)
                            .getStatistic(),searchResult)));
        }
        return SearchResponseDto.builder().country(optionalCountry.get().getName()).response(statisticResponseDtoArrayList).build();
    }

}
