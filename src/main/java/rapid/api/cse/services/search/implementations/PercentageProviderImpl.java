package rapid.api.cse.services.search.implementations;

import org.springframework.stereotype.Component;

@Component
public class PercentageProviderImpl implements rapid.api.cse.services.search.abstractions.PercentageProvider {
    @Override
    public double getPercentage(double forPercentage, double total){
        try{
            return (forPercentage / total) * 100;
        }catch (Exception e){
            return 0d;
        }
    }
}
