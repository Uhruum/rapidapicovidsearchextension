package rapid.api.cse.services.search.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.Search;
import rapid.api.cse.domain.enities.SearchResult;
import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.repositories.SearchResultRepository;
import rapid.api.cse.services.clients.abstractions.HistoryRestClient;
import rapid.api.cse.services.dtos.response.rapidApi.CasesResponseDto;
import rapid.api.cse.services.dtos.response.rapidApi.HistoryResponseDto;
import rapid.api.cse.services.search.abstractions.LongFromStringParser;
import rapid.api.cse.services.search.abstractions.SearchResultStatisticProvider;
import rapid.api.cse.services.search.exceptions.SearchResultProviderException;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SearchResultProviderImpl implements rapid.api.cse.services.search.abstractions.SearchResultProvider {

    private final SearchResultRepository searchResultRepository;
    private final HistoryRestClient historyRestClient;
    private final SearchResultStatisticProvider searchResultStatisticProvider;
    private final LongFromStringParser longFromStringParser;

    @Override
    public Optional<SearchResult> provide(Search search, String date) {
        try {

            List<SearchResult> searchResultArrayList = searchResultRepository.findAllByDateAndCountry(date, search.getCountry());

            if (searchResultArrayList.size() > 0) {
                SearchResult result = searchResultArrayList.get(searchResultArrayList.size() - 1);
                return Optional.of(new SearchResult(result.getNewly(), result.getActive(), result.getCritical(), result.getRecovered(),
                        result.getTotal(), date, search, search.getCountry(), new SearchResultStatistic(result.getStatistic().getNewly(),
                        result.getStatistic().getActive(), result.getStatistic().getCritical(), result.getStatistic().getRecovered())));
            }

            HistoryResponseDto historyResponseDto = historyRestClient.get(search.getCountry().getName(), date);
            if (historyResponseDto.getResponse().isEmpty())
                throw new SearchResultProviderException("History response for country " + search.getCountry().getName() + " and day " + date + " is Empty!");

            CasesResponseDto resultDtoCases = historyResponseDto.getResponse().get(0).getCases();

            return Optional.of(new SearchResult(longFromStringParser.parse(resultDtoCases.getNewly()), resultDtoCases.getActive(), resultDtoCases.getCritical(), resultDtoCases.getRecovered(),
                    resultDtoCases.getTotal(), date, search, search.getCountry(), searchResultStatisticProvider.provide(resultDtoCases)));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Optional.empty();
        }
    }

}
