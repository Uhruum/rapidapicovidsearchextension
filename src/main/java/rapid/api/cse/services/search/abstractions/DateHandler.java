package rapid.api.cse.services.search.abstractions;

import java.util.List;

public interface DateHandler {
    List<String> getDatesBetween(String dateFrom, String dateTo);
}
