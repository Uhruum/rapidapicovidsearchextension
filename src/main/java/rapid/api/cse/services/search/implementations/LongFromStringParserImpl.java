package rapid.api.cse.services.search.implementations;

import org.springframework.stereotype.Component;

@Component
public class LongFromStringParserImpl implements rapid.api.cse.services.search.abstractions.LongFromStringParser {
    @Override
    public long parse(String str){
        if (str.isEmpty() || str.isBlank())
            return 0;
        String potentialLong= str.replaceAll("[^0-9]", "");
        return Long.parseLong(potentialLong);
    }
}
