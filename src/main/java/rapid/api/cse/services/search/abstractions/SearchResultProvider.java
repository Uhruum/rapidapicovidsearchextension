package rapid.api.cse.services.search.abstractions;

import rapid.api.cse.domain.enities.Search;
import rapid.api.cse.domain.enities.SearchResult;

import java.util.Optional;

public interface SearchResultProvider {
    Optional<SearchResult> provide(Search search, String date);
}
