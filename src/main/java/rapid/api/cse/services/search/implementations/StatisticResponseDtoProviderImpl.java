package rapid.api.cse.services.search.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.SearchResult;
import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.dtos.response.search.StatisticResponseDto;

import java.text.DecimalFormat;

@Component
@RequiredArgsConstructor
public class StatisticResponseDtoProviderImpl implements rapid.api.cse.services.search.abstractions.StatisticResponseDtoProvider {

    private final DecimalFormat decimalFormat;

    @Override
    public StatisticResponseDto provide(SearchResultStatistic searchResultStatistic, SearchResult searchResult) {
        return StatisticResponseDto.builder().newly(decimalFormat.format(searchResultStatistic.getNewly()) + "%")
                .active(decimalFormat.format(searchResultStatistic.getActive()) + "%")
                .critical(decimalFormat.format(searchResultStatistic.getCritical()) + "%")
                .recovered(decimalFormat.format(searchResultStatistic.getRecovered()) + "%")
                .day(searchResult.getDate()).build();
    }

}
