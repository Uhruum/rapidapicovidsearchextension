package rapid.api.cse.services.search.abstractions;

public interface PercentageProvider {
    double getPercentage(double forPercentage, double total);
}
