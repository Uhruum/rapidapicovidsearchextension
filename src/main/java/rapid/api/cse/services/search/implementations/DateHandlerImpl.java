package rapid.api.cse.services.search.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
@Component
@RequiredArgsConstructor
public class DateHandlerImpl implements rapid.api.cse.services.search.abstractions.DateHandler {

    private final DateTimeFormatter dateFormatter;

    @Override
    public List<String> getDatesBetween(String dateFrom, String dateTo){
        List<String> dates = new ArrayList<>();
        LocalDate from = LocalDate.parse(dateFrom, dateFormatter);
        LocalDate to = LocalDate.parse(dateTo, dateFormatter);
        Stream<LocalDate> datesUntil = from.datesUntil(to);
        datesUntil.forEach(date->dates.add(date.format(dateFormatter)));
        dates.add(dateTo);
       return dates;
    }

}
