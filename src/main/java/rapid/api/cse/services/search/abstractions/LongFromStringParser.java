package rapid.api.cse.services.search.abstractions;

public interface LongFromStringParser {
    long parse(String str);
}
