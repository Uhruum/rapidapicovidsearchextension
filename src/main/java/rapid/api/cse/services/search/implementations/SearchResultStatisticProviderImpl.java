package rapid.api.cse.services.search.implementations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import rapid.api.cse.domain.enities.SearchResultStatistic;
import rapid.api.cse.services.dtos.response.rapidApi.CasesResponseDto;
import rapid.api.cse.services.search.abstractions.LongFromStringParser;
import rapid.api.cse.services.search.abstractions.PercentageProvider;

@Component
@RequiredArgsConstructor
public class SearchResultStatisticProviderImpl implements rapid.api.cse.services.search.abstractions.SearchResultStatisticProvider {

    private final PercentageProvider percentageProvider;
    private final LongFromStringParser longFromStringParser;

    @Override
    public SearchResultStatistic provide(CasesResponseDto casesResponseDto) {
        SearchResultStatistic statistic = new SearchResultStatistic();
        statistic.setActive(percentageProvider.getPercentage(casesResponseDto.getActive(), casesResponseDto.getTotal()));
        statistic.setCritical(percentageProvider.getPercentage(casesResponseDto.getCritical(), casesResponseDto.getTotal()));
        statistic.setRecovered(percentageProvider.getPercentage(casesResponseDto.getRecovered(), casesResponseDto.getTotal()));
        statistic.setNewly(percentageProvider.getPercentage(longFromStringParser.parse(casesResponseDto.getNewly()), casesResponseDto.getTotal()));
        return statistic;
    }

}
