package rapid.api.cse.services.dtos.response.rapidApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class CasesResponseDto {
    @JsonProperty("new")
    private String newly;
    private long active;
    private long critical;
    private long recovered;
    @JsonProperty("1M_pop")
    private String oneMilionPop;
    private long total;
}
