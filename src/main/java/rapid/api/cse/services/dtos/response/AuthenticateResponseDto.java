package rapid.api.cse.services.dtos.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AuthenticateResponseDto{
    private String token;
}
