package rapid.api.cse.services.dtos.response.search;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
@Data
@Builder
public class SearchResponseDto {
    private String country;
    private ArrayList<StatisticResponseDto> response;
}
