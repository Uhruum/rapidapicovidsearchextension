package rapid.api.cse.services.dtos.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchRequestDto {
    private String country;
    private String dateFrom;
    private String dateTo;
}
