package rapid.api.cse.services.dtos.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class RegisterRequestDto {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
}
