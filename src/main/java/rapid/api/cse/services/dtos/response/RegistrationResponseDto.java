package rapid.api.cse.services.dtos.response;

import lombok.Builder;
import lombok.Data;

@Builder @Data
public class RegistrationResponseDto {
    private String token;
}
