package rapid.api.cse.services.dtos.response.rapidApi;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@Builder
public class ResultDto {
    private String continent;
    private String country;
    private long population;
    private CasesResponseDto cases;
    private DeathsResponseDto deaths;
    private TestsResponseDto tests;
    private String day;
    private Date time;
}
