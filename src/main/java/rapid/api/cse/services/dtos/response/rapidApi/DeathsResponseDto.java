package rapid.api.cse.services.dtos.response.rapidApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class DeathsResponseDto {
    @JsonProperty("new")
    private String newly;
    @JsonProperty("1M_pop")
    private String oneMillionPop;
    private long total;
}
