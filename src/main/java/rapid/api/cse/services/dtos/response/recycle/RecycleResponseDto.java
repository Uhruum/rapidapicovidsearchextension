package rapid.api.cse.services.dtos.response.recycle;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RecycleResponseDto {
    private String message;
    private boolean isSuccessful;
}
