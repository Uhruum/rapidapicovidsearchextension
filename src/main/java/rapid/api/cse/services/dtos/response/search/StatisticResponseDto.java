package rapid.api.cse.services.dtos.response.search;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data @Builder
public class StatisticResponseDto {
    @JsonProperty("new")
    private String newly;
    private String active;
    private String critical;
    private String recovered;
    private String day;
}
