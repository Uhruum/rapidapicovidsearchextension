package rapid.api.cse.services.dtos.response.rapidApi;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@Builder
@EqualsAndHashCode(callSuper=false)
public class HistoryResponseDto extends BaseRapidApiResponseDto {
private ArrayList<ResultDto> response;

    public HistoryResponseDto(ArrayList<ResultDto> response) {
        this.response = response;
    }

    public HistoryResponseDto() {
    }
}
