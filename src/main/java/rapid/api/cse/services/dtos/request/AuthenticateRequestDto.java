package rapid.api.cse.services.dtos.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class AuthenticateRequestDto {
    private String username;
    private String password;
}
