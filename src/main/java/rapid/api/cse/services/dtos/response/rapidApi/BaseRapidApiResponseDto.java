package rapid.api.cse.services.dtos.response.rapidApi;

import lombok.Data;
@Data
public abstract class BaseRapidApiResponseDto {
    private String get;
    private int results;
}
