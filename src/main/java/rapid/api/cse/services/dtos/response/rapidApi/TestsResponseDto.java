package rapid.api.cse.services.dtos.response.rapidApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class TestsResponseDto {
    @JsonProperty("1M_pop")
    private String oneMillionPop;
    private long total;
}
