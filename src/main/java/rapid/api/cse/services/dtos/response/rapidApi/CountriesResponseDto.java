package rapid.api.cse.services.dtos.response.rapidApi;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@Builder
@EqualsAndHashCode(callSuper=false)
public class CountriesResponseDto extends BaseRapidApiResponseDto {
    private ArrayList<String> response;

    public CountriesResponseDto(ArrayList<String> response) {
        this.response = response;
    }

    public CountriesResponseDto() {
    }
}
