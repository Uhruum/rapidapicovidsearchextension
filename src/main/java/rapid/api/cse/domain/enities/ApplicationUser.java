package rapid.api.cse.domain.enities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity @Getter
@Setter
@NoArgsConstructor
@Table(
uniqueConstraints = {
        @UniqueConstraint(columnNames = "username")
})
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String firstName;
    private String lastName;

    @OneToMany(mappedBy = "applicationUser")
    private Set<Search> searches;

    public ApplicationUser(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

