package rapid.api.cse.domain.enities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity @Getter
@Setter
@NoArgsConstructor
@Table(name = "search_result_statistic")
public class SearchResultStatistic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double newly;
    private double active;
    private double critical;
    private double recovered;

    @OneToOne(mappedBy = "statistic", fetch = FetchType.EAGER)
    @Fetch(value= FetchMode.SELECT)
    private SearchResult searchResult;

    public SearchResultStatistic(double newly, double active, double critical, double recovered) {
        this.newly = newly;
        this.active = active;
        this.critical = critical;
        this.recovered = recovered;
    }
}
