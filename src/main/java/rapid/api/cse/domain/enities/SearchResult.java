package rapid.api.cse.domain.enities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor @Getter @Setter
@Table(name = "search_results")
public class SearchResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long newly;
    private long active;
    private long critical;
    private long recovered;
    private long total;
    private String date;

    @ManyToOne
    @JoinColumn(name = "search_id", nullable = false)
    private Search search;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    private Country country;

    @OneToOne
    @JoinColumn(name = "search_result_statistic_id", referencedColumnName = "id", nullable = false)
    private SearchResultStatistic statistic;

    public SearchResult(long newly, long active, long critical, long recovered, long total, String date, Search search, Country country, SearchResultStatistic statistic) {
        this.newly = newly;
        this.active = active;
        this.critical = critical;
        this.recovered = recovered;
        this.total = total;
        this.date = date;
        this.search = search;
        this.country = country;
        this.statistic = statistic;
    }
}
