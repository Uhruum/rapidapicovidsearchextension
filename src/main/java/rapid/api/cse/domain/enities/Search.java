package rapid.api.cse.domain.enities;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity @Getter
@Setter
@NoArgsConstructor
@Table(name = "searches")
public class Search {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String dateFrom;

    private String dateTo;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable=false)
    private Country country;

    @ManyToOne
    @JoinColumn(name = "app_user_id", nullable=false)
    private ApplicationUser applicationUser;

    @CreationTimestamp
    private Timestamp inserted;

    @OneToMany(mappedBy = "search", fetch = FetchType.EAGER)
    @Fetch(value= FetchMode.SELECT)
    private Set<SearchResult> results;

    public Search(String dateFrom, String dateTo, Country country, ApplicationUser applicationUser) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.country = country;
        this.applicationUser = applicationUser;
    }
}
