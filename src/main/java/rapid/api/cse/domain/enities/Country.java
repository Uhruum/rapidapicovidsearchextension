package rapid.api.cse.domain.enities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity @Getter
@Setter
@NoArgsConstructor
@Table(name = "countries")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "country")
    private Set<Search> searches;

    @OneToMany(mappedBy = "country")
    private Set<SearchResult> searchResults;

    public Country(String name) {
        this.name = name;
    }
}
